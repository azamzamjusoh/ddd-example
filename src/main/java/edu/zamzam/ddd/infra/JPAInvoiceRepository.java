package edu.zamzam.ddd.infra;

import edu.zamzam.ddd.domain.Invoice;
import edu.zamzam.ddd.domain.InvoiceRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository ("jpaInvoiceRepository")
public class JPAInvoiceRepository implements InvoiceRepository {

    @Override
    public Invoice get(Integer id) {
        return null;
    }

    @Override
    public void add(Invoice invoice) {

    }

    @Override
    public Collection<Invoice> getAll() {
        return null;
    }

    @Override
    public boolean remove(Invoice invoice) {
        return false;
    }
}
