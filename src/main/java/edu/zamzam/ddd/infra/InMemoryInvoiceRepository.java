package edu.zamzam.ddd.infra;

import edu.zamzam.ddd.domain.Invoice;
import edu.zamzam.ddd.domain.InvoiceRepository;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository("inMemoryInvoiceRepository")
public class InMemoryInvoiceRepository implements InvoiceRepository {

    private static int idSequence = 1;
    private static Map<Integer, Invoice> map = new LinkedHashMap<>();
    @Override
    public Invoice get(Integer id) {
        return map.get(id);
    }

    @Override
    public void add(Invoice invoice) {
        if (invoice.getId() == null) {
            int id = getNextId();
            if (map.get(id) == null) {
                invoice.setId(id);
                map.put(id, invoice);
            } else {
                throw new IllegalStateException("Id duplicates");
            }
        } else {
            throw new IllegalArgumentException("id field must be null");
        }
    }

    @Override
    public Collection<Invoice> getAll() {
        return Collections.unmodifiableCollection(map.values());
    }

    @Override
    public boolean remove(Invoice invoice) {
        Invoice existing = map.remove(invoice.getId());
        return existing!=null;
    }

    private int getNextId(){
        return idSequence++;
    }
}
