package edu.zamzam.ddd.domain;


import java.util.Date;

public class InvoicePayment {

    private Integer id;
    private Integer invoiceId;
    private Invoice invoice;
    private Integer paymentId;
    private Payment payment;
    private Money allocation;
    private Date dateCreated;

    public InvoicePayment(){}

    public InvoicePayment(Invoice invoice, Payment payment, Money allocation, Date dateCreated) {
        super();
        if (!allocation.equalsCurrency(payment.getAmount())){
            throw new IllegalArgumentException("Currency must be identical");
        }
        if (allocation.amountGreaterThan(payment.getAmount())){
            throw new IllegalArgumentException("Allocation amount cannot be greater the payment");
        }
        this.invoice = invoice;
        this.payment = payment;
        this.allocation = allocation;
        this.dateCreated = dateCreated;
    }

    public Integer getId() {
        return id;
    }

    public Invoice getInvoice() {
        return invoice;
    }

    public Payment getPayment() {
        return payment;
    }

    public Money getAllocation() {
        return allocation;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        InvoicePayment other = (InvoicePayment) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        return true;
    }

}
