package edu.zamzam.ddd.domain;

import java.util.Date;

public class Payment extends Tx {

    private String receiptNumber;
    private PaymentMethod paymentMethod;
    private Money amountUsed;
    private String remark;

    public Payment() {
    }

    public Payment(String description, Money amount, Date dateTransacted, PaymentMethod paymentMethod) {
        super(TxType.CREDIT, description, amount, dateTransacted);
        //amount cannot be negative
        if (amount.isNegative()) {
            throw new IllegalArgumentException("Payment cannot be negative value: " + amount);
        }
        this.paymentMethod = paymentMethod;
        //initially amountUsed is zero
        this.amountUsed = new Money(amount.getCurrency(), Money.ZERO);
    }

    public String getReceiptNumber() {
        return receiptNumber;
    }

    public void setReceiptNumber(String receiptNumber) {
        this.receiptNumber = receiptNumber;
    }

    public PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public Money getAmountUsed() {
        return amountUsed;
    }

    public Money getAmountUnUsed() {
        return amount.substract(amountUsed);
    }

    public boolean isAllUsed() {
        boolean allUsed = getAmountUnUsed().getAmount().compareTo(Money.ZERO) == 0;
        return allUsed;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getRemark() {
        return remark;
    }

    /**
     * Each time payment is made against an invoice, must call this method to update its the amount used to
     * pay for the invoice
     *
     * @param money
     */
    void use(Money money) {
        //make a copy
        Money _amountUse = new Money(amountUsed);
        _amountUse = _amountUse.add(money);
        if (_amountUse.amountGreaterThan(amount)) {
            throw new IllegalArgumentException("Cannot use more than what the payment have: amount=" + amount +
                    ", amountUsed=" + _amountUse);
        }
        amountUsed = _amountUse;
    }

}
