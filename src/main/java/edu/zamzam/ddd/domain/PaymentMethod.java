package edu.zamzam.ddd.domain;

public enum PaymentMethod {
    CASH, CREDITCARD, BANKTRANSFER
}
