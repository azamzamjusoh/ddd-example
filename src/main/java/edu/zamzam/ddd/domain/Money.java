package edu.zamzam.ddd.domain;

import java.math.BigDecimal;

public class Money {

    public static final BigDecimal ZERO = new BigDecimal("0.00");
    private String currency;
    private BigDecimal amount;

    public Money() {
        this.currency = "DEFAULT";
        this.amount = ZERO;
    }

    public Money(String currency, BigDecimal amount) {
        super();
        this.currency = currency;
        if (this.currency == null) {
            throw new IllegalArgumentException("currency cannot be null or empty or blank String: " + currency);
        }
        if (amount == null) {
            throw new IllegalArgumentException("amount cannot be null");
        }
        this.amount = amount;
    }

    public Money(Money copy) {
        this.currency = copy.getCurrency();
        this.amount = copy.getAmount();
    }

    public Money(String currency, String amount) {
        super();
        this.currency = currency;
        this.amount = new BigDecimal(amount);
    }

    public String getCurrency() {
        return currency;
    }

    /**
     * @return
     */
    public BigDecimal getAmount() {
        return amount;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((amount == null) ? 0 : amount.hashCode());
        result = prime * result + ((currency == null) ? 0 : currency.hashCode());
        return result;
    }

    /**
     * Two money is considered equal of
     * 1 - its currency is equal
     * 2 - its amount is equal using compareTo method
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Money other = (Money) obj;
        if (amount == null) {
            if (other.amount != null)
                return false;
        } else if (amount.compareTo(other.amount) != 0)
            return false;
        if (currency == null) {
            if (other.currency != null)
                return false;
        } else if (!currency.equals(other.currency))
            return false;
        return true;
    }

    public boolean isNegative() {
        return this.amount.compareTo(ZERO) < 0;
    }

    public boolean equalsCurrency(Money money) {
        return this.currency.equals(money.getCurrency());
    }

    public boolean amountLessThan(Money money) {
        return amount.compareTo(money.getAmount()) < 0;
    }

    public boolean amountLessEqualThan(Money money) {
        return amount.compareTo(money.getAmount()) <= 0;
    }

    public boolean amountGreaterThan(Money money) {
        return amount.compareTo(money.getAmount()) > 0;
    }

    public boolean amountGreaterEqualThan(Money money) {
        return amount.compareTo(money.getAmount()) >= 0;
    }

    public Money add(Money money) {
        //if (!equalsCurrency(money)){
        //	throw new IllegalArgumentException("Currency must be identical for addition operation");
        //}
        return new Money(this.currency, this.getAmount().add(money.getAmount()));
    }

    public Money substract(Money money) {
        if (!equalsCurrency(money)) {
            throw new IllegalArgumentException("Currency must be identical for substract operation");
        }
        return new Money(this.currency, this.getAmount().subtract(money.getAmount()));
    }

    public Money multiplyPercent(BigDecimal percent) {
        return new Money(this.currency, this.getAmount().multiply(percent).multiply(new BigDecimal("0.01")));
    }

    public Money negate() {
        return new Money(this.currency, this.amount.negate());
    }

    @Override
    public String toString() {
        return "[" + currency + " " + amount + "]";
    }

}
