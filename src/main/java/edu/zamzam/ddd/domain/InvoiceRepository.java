package edu.zamzam.ddd.domain;

import java.util.Collection;

public interface InvoiceRepository {

    Invoice get(Integer id);

    void add(Invoice invoice);

    Collection<Invoice> getAll();

    boolean remove(Invoice invoice);
}
