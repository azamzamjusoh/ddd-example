package edu.zamzam.ddd.domain;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;


public class Invoice {

    private Integer id;
    private Date dateCreated;
    private String description;
    private String currency = "RM";
    private List<InvoiceItem> items;
    private List<InvoicePayment> payments;

    @SuppressWarnings("unused")
    private Invoice(){}

    public Invoice(Date dateCreated, String description) {
        super();
        this.dateCreated = dateCreated;
        this.description = description;
        this.items = new ArrayList<InvoiceItem>();
        this.payments = new ArrayList<InvoicePayment>();
    }

    public Integer getId() {
        return id;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public List<InvoiceItem> getItems() {
        return Collections.unmodifiableList(items);
    }

    public List<InvoicePayment> getPayments() {
        return Collections.unmodifiableList(payments);
    }


    /**
     * Get total amount of payment made to this invoice
     * @return
     */
    public Money getTotalPaid(){
        Money sum = new Money(currency, "0.00");
        for (InvoicePayment ip: payments){
            sum = sum.add(ip.getAllocation());
        }
        return sum;
    }

    /**
     * Get the total amount owed (i.e. as in invoiceItems)
     * @return
     */
    public Money getTotalOwed(){
        Money sum = new Money(currency, "0.00");
        for (InvoiceItem ip: items){
            sum = sum.add(ip.getAmount());
        }
        return sum;
    }

    public boolean isFullyPaid(){
        return getTotalOwed().equals(getTotalPaid());
    }

    /**
     * Get the total unpaid balance of this invoice
     * @return
     */
    public Money getUnpaidBalance(){
        return getTotalOwed().substract(getTotalPaid());
    }

    /**
     * Make a payment to this invoice - the amount of money in the
     * payment object not necessarily equal to the balance in the
     * invoice.
     * @param payment - the Payment instance
     * @return
     */
    public InvoicePayment pay(Payment payment){
        if (paymentAlreadyExist(payment)){
            throw new IllegalStateException("Payment object already exist in the invoice");
        }
        if (!isFullyPaid()){ //only for not fully paid invoice
            Money balance = getUnpaidBalance();

            if (balance.amountLessEqualThan(payment.getAmountUnUsed())){
                //if balance unpaid is less or equal than the payment unused amount
                //then pay up to the balance amount
                InvoicePayment ip = new InvoicePayment(this, payment, balance, new Date());
                payment.use(balance);
                this.payments.add(ip);
                return ip;
            }
            else {
                //if balance unpaid is more than the payment unused amount
                //then pay up to the payment amount
                InvoicePayment ip = new InvoicePayment(this, payment, payment.getAmountUnUsed(),
                        new Date());
                payment.use(payment.getAmountUnUsed());
                this.payments.add(ip);
                return ip;
            }

        }
        return null;
    }

    /**
     * Check if the Payment object already added to this invoice (i.e. already used to pay for
     * this invoice)
     * @param p
     * @return
     */
    public boolean paymentAlreadyExist(Payment p){
        return payments.indexOf(p)!=-1;
    }

    public InvoiceItem addItem(String description, Money amount, Date dateTransacted){
        InvoiceItem newItem = new InvoiceItem(this, description, amount, dateTransacted);
        this.items.add(newItem);
        return newItem;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCurrency() {
        return currency;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
