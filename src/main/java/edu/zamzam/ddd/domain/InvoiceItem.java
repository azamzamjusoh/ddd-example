package edu.zamzam.ddd.domain;

import java.util.Date;


public class InvoiceItem extends Tx {

    private Integer invoiceId;
    private Invoice invoice;

    public InvoiceItem(){
    }

    InvoiceItem(Invoice invoice, String description, Money amount, Date dateTransacted) {
        super(TxType.DEBIT, description, amount, dateTransacted);
        this.invoice = invoice;
    }

    public Invoice getInvoice() {
        return invoice;
    }

}

