package edu.zamzam.ddd.domain;

import java.util.Date;

public abstract class Tx {

    protected Integer id;
    protected TxType type;
    protected String description;
    protected Money amount;
    protected Date dateTransacted;

    public Tx(){
    }

    public Tx(TxType type, String description, Money amount, Date dateTransacted) {
        super();
        this.type = type;
        this.description = description;
        this.amount = amount;
        this.dateTransacted = dateTransacted;
    }

    public Integer getId() {
        return id;
    }

    public TxType getType() {
        return type;
    }

    public String getDescription() {
        return description;
    }

    public Money getAmount() {
        return amount;
    }

    public Date getDateTransacted() {
        return dateTransacted;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Tx other = (Tx) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        return true;
    }


}

