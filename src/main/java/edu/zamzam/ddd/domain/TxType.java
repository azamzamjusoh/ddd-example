package edu.zamzam.ddd.domain;

public enum TxType {
    DEBIT, CREDIT;
}
