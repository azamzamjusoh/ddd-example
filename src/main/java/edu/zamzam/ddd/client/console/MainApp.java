package edu.zamzam.ddd.client.console;

import edu.zamzam.ddd.app.ApplicationService;
import edu.zamzam.ddd.app.InvoiceDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Collection;
import java.util.Date;

public class MainApp {
    static Logger logger = LoggerFactory.getLogger(MainApp.class);

    @Autowired
    private ApplicationService applicationService;

    private MainApp(ApplicationContext applicationContext){
        AutowireCapableBeanFactory beanFactory = applicationContext.getAutowireCapableBeanFactory();
        beanFactory.autowireBean(this);
    }
    public static void main(String[] args) throws Exception {
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        MainApp app = new MainApp(context);
        app.run();
    }

    public void run() throws IOException {
        System.out.println("Enter command > ");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String command = reader.readLine();
        while (!command.equalsIgnoreCase("Q")) {
            if (command.equalsIgnoreCase("C")) {
                handleCreate(reader);
            }
            else if (command.equalsIgnoreCase("L")) {
                handleList();
            }
            else if (command.equalsIgnoreCase("E")) {
                handleEdit(reader);
            }
            else if (command.equalsIgnoreCase("D")) {
                handleDelete(reader);
            }
            System.out.println("Enter command > ");
            command = reader.readLine();
        }
    }

    private void handleEdit(BufferedReader reader) throws  IOException{
        System.out.println("Enter invoice ID to edit: ");
        String invoiceId = reader.readLine();
        InvoiceDTO invoiceDTO = applicationService.getInvoice(new Integer(invoiceId));
        if (invoiceDTO != null) {
            System.out.println("Enter new description for Invoice" + invoiceId);
            String description = reader.readLine();
            if (description.trim().length()>0) {
                invoiceDTO.setDescription(description);
                applicationService.update(invoiceDTO);
                System.out.println("Invoice " + invoiceDTO + " updated");
            }
        }

    }

    private void handleCreate(BufferedReader reader) throws IOException{
        System.out.println("Enter invoice description: ");
        String description = reader.readLine();
        if (description.trim().length()>0) {
            InvoiceDTO invoiceDTO = applicationService.createInvoice(description, new Date());
            System.out.println("Invoice " + invoiceDTO + " created");
        }
    }

    private void handleDelete(BufferedReader reader) throws IOException{
        System.out.println("Enter invoice ID to delete: ");
        String invoiceId = reader.readLine();
        if (applicationService.deleteInvoice(new Integer(invoiceId))){
            System.out.println("invoice " + invoiceId + " deleted");
        } else {
            System.out.println("Fail to delete invoice " + invoiceId);
        }
    }

    private void handleList(){
       Collection<InvoiceDTO> allInvoices = applicationService.getAllInvoice();
       if (allInvoices.size() == 0) {
           System.out.println("No invoice");
           return;
       }
       System.out.println("ID\tDescription\tDate created\t\t\t\tTotal Owed");
       for (InvoiceDTO invoiceDTO : allInvoices) {
           System.out.println(invoiceDTO.getId() + "\t" + invoiceDTO.getDescription() + "\t" + invoiceDTO.getDateCreated() + "\t" + invoiceDTO.getTotalOwed());
       }
    }
}
