package edu.zamzam.ddd.app;

import java.util.Date;

public class InvoiceItemDTO {

    private Integer id;
    private String description;
    private MoneyDTO amount;
    private Date dateTransacted;

    public InvoiceItemDTO(Integer id, String description, MoneyDTO amount, Date dateTransacted) {
        this.id = id;
        this.description = description;
        this.amount = amount;
        this.dateTransacted = dateTransacted;
    }

    public Integer getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public MoneyDTO getAmount() {
        return amount;
    }

    public Date getDateTransacted() {
        return dateTransacted;
    }

    @Override
    public String toString() {
        return "InvoiceItemDTO{" +
                "id=" + id +
                ", description='" + description + '\'' +
                ", amount=" + amount +
                ", dateTransacted=" + dateTransacted +
                '}';
    }
}
