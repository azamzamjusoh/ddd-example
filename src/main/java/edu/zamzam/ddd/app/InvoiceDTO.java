package edu.zamzam.ddd.app;

import java.io.Serializable;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class InvoiceDTO implements Serializable {

    private Integer id;
    private Date dateCreated;
    private String description;
    private String currency;
    private List<InvoiceItemDTO> invoiceItems = Collections.EMPTY_LIST;
    private String totalOwed;

    public InvoiceDTO(Integer id, Date dateCreated, String description, String currency, String totalOwed, List<InvoiceItemDTO> invoiceItems) {
        this.id = id;
        this.dateCreated = dateCreated;
        this.description = description;
        this.currency = currency;
        this.invoiceItems = invoiceItems;
        this.totalOwed = totalOwed;
    }

    public Integer getId() {
        return id;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCurrency() {
        return currency;
    }

    public List<InvoiceItemDTO> getInvoiceItems() {
        return Collections.unmodifiableList(invoiceItems);
    }

    public void setInvoiceItems(List<InvoiceItemDTO> invoiceItems) {
        if (invoiceItems != null) {
            this.invoiceItems = invoiceItems;
        } else  {
            throw new IllegalArgumentException("invoiceItems cannot be null");
        }

    }

    public String getTotalOwed() {
        return totalOwed;
    }

    @Override
    public String toString() {
        return "InvoiceDTO{" +
                "id=" + id +
                ", dateCreated=" + dateCreated +
                ", description='" + description + '\'' +
                ", currency='" + currency + '\'' +
                '}';
    }
}
