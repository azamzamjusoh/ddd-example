package edu.zamzam.ddd.app;

public class MoneyDTO {

    private String currency;
    private String amount;

    public MoneyDTO(String currency, String amount) {
        this.currency = currency;
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public String getAmount() {
        return amount;
    }
}
