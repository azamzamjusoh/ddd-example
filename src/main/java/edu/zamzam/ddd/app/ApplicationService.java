package edu.zamzam.ddd.app;

import edu.zamzam.ddd.domain.Invoice;
import edu.zamzam.ddd.domain.InvoiceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.stream.Collectors;

@Service
public class ApplicationService {

    private InvoiceRepository invoiceRepository;

    @Autowired
    public ApplicationService(InvoiceRepository invoiceRepository) {
        this.invoiceRepository = invoiceRepository;
    }

    public InvoiceDTO createInvoice(String invDesc, Date date) {
        Invoice invoice = new Invoice(date, invDesc);
        invoiceRepository.add(invoice);
        return new InvoiceDTO(invoice.getId(), invoice.getDateCreated(), invoice.getDescription(), invoice.getCurrency(), invoice.getTotalOwed().toString(), new ArrayList<>());
    }

    public InvoiceDTO getInvoice(Integer id) {
        Invoice invoice = invoiceRepository.get(id);
        if (invoice != null) {
            //FIXME convert InvoiceItem to InvoiceItemDto
            return new InvoiceDTO(invoice.getId(), invoice.getDateCreated(), invoice.getDescription(), invoice.getCurrency(), invoice.getTotalOwed().toString(), new ArrayList<>());
        }
        return null;
    }

    public Collection<InvoiceDTO> getAllInvoice() {
        return invoiceRepository.getAll().stream().map(invoice -> new InvoiceDTO(invoice.getId(), invoice.getDateCreated(), invoice.getDescription(), invoice.getCurrency(), invoice.getTotalOwed().toString(), new ArrayList<>())).collect(Collectors.toList());
    }

    public boolean deleteInvoice(Integer invoiceId) {
        Invoice invoice = invoiceRepository.get(invoiceId);
        if (invoice != null) {
            return invoiceRepository.remove(invoice);
        }
        return false;
    }

    public void update(InvoiceDTO invoiceDTO) {
        Invoice invoice = invoiceRepository.get(invoiceDTO.getId());
        if (invoice != null) {
            invoice.setDescription(invoiceDTO.getDescription());
        }
    }
}
