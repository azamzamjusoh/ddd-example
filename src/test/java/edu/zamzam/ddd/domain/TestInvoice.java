package edu.zamzam.ddd.domain;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

public class TestInvoice {

    static Logger logger = LoggerFactory.getLogger(TestInvoice.class);

    @Test
    public void testCreateInvoiceWithItems() throws Exception {

        //creates first invoice ---------------------------------------
        Invoice inv1 = new Invoice(new Date(), "C701/66666/test");
        InvoiceItem item1 = inv1.addItem("NON RECURRING FEE", new Money("RM", "300.00"),
                new Date());

        InvoiceItem item2 = inv1.addItem("RECURRING FEE", new Money("RM", "170.00"),
                new Date());

        //the total item amount is suppose to be
        Money expectedOwed = new Money("RM", "470.00");
        //validate its values
        assertEquals(expectedOwed, inv1.getTotalOwed());

        //creates second invoice  ---------------------------------------
        Calendar cal = new GregorianCalendar();
        cal.add(Calendar.DATE, -31);
        //add invoice with date -31 days ago
        Invoice inv2 = new Invoice(cal.getTime(), "C701/66667/test");
        InvoiceItem item2_1 = inv2.addItem("NON RECURRING FEE", new Money("RM", "200.00"),
                cal.getTime());

        InvoiceItem item2_2 = inv2.addItem("RECURRING FEE", new Money("RM", "70.00"),
                cal.getTime());

        //the total item amount is suppose to be
        expectedOwed = new Money("RM", "270.00");
        //validate its values
        assertEquals(expectedOwed, inv2.getTotalOwed());
    }

    /**
     * Create an invoice 'inv1' with 2 items with total of RM 219.51.
     * Then make partial payments of RM 100, RM 100, and RM 300 to the invoice.
     * Then add another invoice 'inv2' with single item with total of 300.00.
     * Then pay the second invoice using the remaining amount from the 3rd payment.
     * <p>
     * Basically this test cases where single invoice paid partially by multiple payment,
     * and a single payment can pay for multiple invoices.
     * <p>
     */
    @Test
    public void testInvoicePayment() {

        //add an invoice
        Invoice inv1 = new Invoice(new Date(), "Test Invoice 01");
        logger.debug("Create a new invoice: " + inv1);
        //add 2 items
        InvoiceItem item1 = inv1.addItem("Registration Fee", new Money(Constant.CURRENCY_RM, "120.51"),
                new Date());
        logger.debug("Adding an item1 to invoice 1: " + item1);
        InvoiceItem item2 = inv1.addItem("Tution Fee", new Money(Constant.CURRENCY_RM, "99.00"),
                new Date());
        logger.debug("Adding an item2 to invoice 1: " + item2);


        //the total item amount is suppose to be
        Money expectedOwed = new Money(Constant.CURRENCY_RM, "219.51");
        //validate its values
        assertEquals(expectedOwed, inv1.getTotalOwed());
        logger.debug("inv1:total owed: " + inv1.getTotalOwed());
        logger.debug("inv1:total paid: " + inv1.getTotalPaid());

        //now we make a first payment of RM 100
        Money amt1 = new Money(Constant.CURRENCY_RM, "100");
        Payment p1 = new Payment("Partial payment 1", amt1, new Date(), PaymentMethod.CASH);

        //invoice will accept the payment
        logger.debug("Make a first payment to inv1: " + p1.getAmount());
        inv1.pay(p1);

        //check if the amount tally
        //219.51 - 100 = 119.51
        Money expectedBalance = new Money(Constant.CURRENCY_RM, "119.51");
        assertEquals(expectedBalance, inv1.getUnpaidBalance());
        // total paid is 100
        Money expectedTotalPaid = new Money(Constant.CURRENCY_RM, "100");
        assertEquals(expectedTotalPaid, inv1.getTotalPaid());
        logger.debug("inv1:total owed: " + inv1.getTotalOwed());
        logger.debug("inv1:total paid: " + inv1.getTotalPaid());
        logger.debug("Money left (unused) from 2nd payment: " + p1.getAmountUnUsed());

        //now we make a second payment - pay another 100
        Money amt2 = new Money(Constant.CURRENCY_RM, "100");
        Payment p2 = new Payment("Partial payment 2", amt2, new Date(), PaymentMethod.CASH);

        //invoice will accept the payment
        logger.debug("Make a second payment to inv1: " + p2.getAmount());
        inv1.pay(p2);

        //check if the amount tally
        //119.51 - 100 = 19.51
        expectedBalance = new Money(Constant.CURRENCY_RM, "19.51");
        assertEquals(expectedBalance, inv1.getUnpaidBalance());
        // total paid is 200
        expectedTotalPaid = new Money(Constant.CURRENCY_RM, "200");
        assertEquals(expectedTotalPaid, inv1.getTotalPaid());
        logger.debug("inv1:total owed: " + inv1.getTotalOwed());
        logger.debug("inv1:total paid: " + inv1.getTotalPaid());
        logger.debug("Money left (unused) from 2nd payment: " + p2.getAmountUnUsed());


        //now we make a third payment - pay another 300
        Money amt3 = new Money(Constant.CURRENCY_RM, "300");
        Payment p3 = new Payment("Partial payment 3", amt3, new Date(), PaymentMethod.CASH);

        //inv1 will accept the payment
        logger.debug("Make a third payment to inv1: " + p3.getAmount());
        inv1.pay(p3);

        logger.debug("inv1:total owed: " + inv1.getTotalOwed());
        logger.debug("inv1:total paid: " + inv1.getTotalPaid());
        logger.debug("Money left (unused) from 3rd payment: " + p3.getAmountUnUsed());


        //add a second invoice
        Invoice inv2 = new Invoice(new Date(), "Test Invoice 02");
        logger.debug("Create a second invoice: " + inv2);
        //add 2 items
        item1 = inv2.addItem("Second Semester Fee", new Money(Constant.CURRENCY_RM, "300"),
                new Date());
        logger.debug("Adding an item1 to invoice 2: " + item1);

        logger.debug("inv2:total owed: " + inv2.getTotalOwed());
        logger.debug("inv2:total paid: " + inv2.getTotalPaid());


        logger.debug("Make a payment to inv2, using whatever balance from the 3rd payment: " + p3.getAmountUnUsed());
        inv2.pay(p3);
        logger.debug("inv2:total owed: " + inv2.getTotalOwed());
        logger.debug("inv2:total paid: " + inv2.getTotalPaid());
        logger.debug("Money left (unused) from 3rd payment: " + p3.getAmountUnUsed());

    }


}
